---
layout: markdown_page
title: "Specialist"
---

Specialists carry responsibility for a certain topic.
They keep track of issues in this topic and/or spend the majority of their time there.
Commonly there is a lead in this topic that makes the final calls.
