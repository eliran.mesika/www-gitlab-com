---
layout: markdown_page
title: "Developer Advocacy - Public Speaking"
---

Public speaking is one of the best ways to get in front of people. 

## Where to Speak

There are a few places where you can keep tabs on open Calls for Papers.

- Papercall.io
- The CFP Report

In general, try to avoid sponsored speaking slots/slots we have to pay to get. There's value in these, but attendance rates are lower than organic conference talks.

## Talk Preparation

### Abstracts

- Keep [these guidelines](https://medium.com/@housecor/conference-speaker-here-s-7-tips-for-getting-accepted-6151af513148) in mind when coming up with talk ideas.
- Say what you plan to cover. What will attendees get out of this talk?
- Try to keep abstracts short unless the conference organizers request a long abstract. They're typically what attendees see in the program/conference guide.
- Submit to [Help Me Abstract](http://helpmeabstract.com/) for feedback.

### Slides

- Provide attendees with links to your slides, notes, code samples, etc. on one of your first slides. Repeat on one of the last ones.
- If you link to things in your slides, make those links available outside of your slides in a notes section.
- Provide contact info on your slides. (Name, Email (optional), Twitter handle, GitLab handle)
- Put one contact method at the bottom of each slide. Twitter is a solid choice.

### Practice!

- Prepare for your talk as if the tech you depend on will fail.
- You should spend a few hours rehearsing what you will say.
- Figure out where your potential pitfalls are and make sure those sections get ample attention.
- Prepare your talk as if all technology will die that day and you'll be forced to give it in the dark as a Q&A session. This includes not having Internet.

### At the Event

- Have your slides on your computer, the Internet, and on a thumb drive.
- The thumb drive is also a useful backup location for code samples.
- Bring your own adapters, laptop, and charging cables.
- Don't expect a reliable Internet connection.
- Make sure your presentation works in a variety of screen resolutions (including 800x600).

## Speaking

- Thank the organizers and the sponsors.
- Be yourself, but remember you're representing GitLab.
- If things go wrong (and they will!), don't let it ruin your day. Keep moving forward.
- Ask the audience questions to keep them engaged. "How many of you have done $x?" or "Who here is familiar with $x?"
- Allow Q&A sessions

### Q&A

If you do a Q&A session, it's important to maintain control of it so that everyone gets a chance to ask questions they may have.

- If you don't know an answer, saying so is fine.
- Ask if someone else in the audience has the answer. If no one says anything, follow up with the person offline.
- Don't lie your way through these sessions.


### Post-Talk

- Send follow-up emails to anyone who had questions that went unanswered/anyone who gave you a business card.
- Upload your slides/code/videos as soon as you can.

## Talk Debrief

How was the talk received by the audience?
What went well?
What didn't go so well?
Were there any technical difficulties?
What sort of questions were asked during your Q&A session?
